import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";

export default class loginFacebook extends Component {
  state = {
    isLoggedIn: false,
    userID: "",
    name: "",
    email: "",
    picture: " "
  };

  responseFacebook = respone => {
    console.log(respone);
    this.setState({
      isLoggedIn: true,
      userID: respone.userID,
      name: respone.name,
      email: respone.email,
      picture: respone.picture.data.url
    });
  };

  componentClicked = () => {
    console.log("clicked");
  };

  render() {
    let fbContent;
    if (this.state.isLoggedIn) {
      fbContent = (
        <div
          style={{
            width: '400px',
            margin: 'auto',
            background: '#f4f4f4',
            padding: '20px'
          }}
        >
            <img src={this.state.picture} alt={this.state.name}></img>
            <h2>Chào mừng {this.state.name}</h2>
            Email: {this.state.email}
        </div>
      );
    } else {
      fbContent = (
        <FacebookLogin
          appId="648461539012173"
          autoLoad={true}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      );
    }
    return <div>{fbContent}</div>;
  }
}
